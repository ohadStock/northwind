import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CountriesComponent } from './app/countries/countries.component';
import { CityComponent } from './app/city/city.component';

const routes: Routes = [
  { path: '', redirectTo: '/countries', pathMatch: 'full' },
  {path: 'countries', component: CountriesComponent },
  {path: 'city/:id', component: CityComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
