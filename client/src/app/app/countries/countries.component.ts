import { ICustomersInitialState, CountriesSelector, CountriesList } from './../reducers/customers.reducer';
import { Component, OnInit } from '@angular/core';
import { FetchCountries } from '../actions/customers.actions';
import { Store } from '@ngrx/store';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {
  public countries$ = this.store.select(CountriesSelector)
  public countriesList: string[];
  public allCountries: CountriesList;

  constructor(public store: Store<ICustomersInitialState>) {
    this.store.dispatch(new FetchCountries());
  }

  ngOnInit() {
    
    this.countries$.subscribe(countries => {
      this.countriesList = Object.keys(countries);
      this.allCountries = countries;
    });
  }

}
