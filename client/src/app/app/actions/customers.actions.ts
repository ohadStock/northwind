import { Action } from '@ngrx/store';
import { Customer } from '../reducers/customers.reducer';


    export enum CustomersActionTypes {
        FETCH_COUNTRIES = '[Customers] Fetch Countries',
        FETCH_COUNTRIES_SUCCESS = '[Customers] Fetch Countries Success',
        FETCH_CITY = '[Customers] Fetch city',
        FETCH_CITY_SUCCESS = '[Customers] Fetch city success',
        SELECT_CITY = '[Customers] Select City',
    }

    export class FetchCountries implements Action {
        readonly type = CustomersActionTypes.FETCH_COUNTRIES;
    }

    export class FetchCountriesSuccess implements Action {
        readonly type = CustomersActionTypes.FETCH_COUNTRIES_SUCCESS;
        constructor(public payload: { [key: string]: { id: string, name: string }[] }) {}
    }

    export class FetchCity implements Action {
        readonly type =  CustomersActionTypes.FETCH_CITY;
        constructor(public id: string) {}
    }

    export class FetchCitySusccess implements Action {
        readonly type = CustomersActionTypes.FETCH_CITY_SUCCESS;
        constructor(public payload: Customer) {}
    }

    export class SelectCity implements Action {
        readonly type = CustomersActionTypes.SELECT_CITY;
        constructor(public payload: string) {}
    }

