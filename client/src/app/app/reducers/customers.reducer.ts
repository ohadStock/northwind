import { SelectCity } from './../actions/customers.actions';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { FetchCitySusccess, FetchCountriesSuccess, CustomersActionTypes } from '../actions/customers.actions';


export interface Customer {
    Id: string;
    CompanyName: string;
    ContactName: string;
    ContactTitle: string;
    Address: string;
    City: string;
    PostalCode: string;
    Country: string;
    Phone: string;
    Fax: string;
}

export interface CountriesList {
    [key: string]: { id: string, name: string }[]; 
}

export interface ICustomersInitialState {
    countries: CountriesList;
    cities:  {[key: string]: Customer};
    selectedCity: string;
}

export const customersInitialState = {
    countries: [],
    selectedCity: undefined,
    cities: {}
};

export function customersReducer(state = customersInitialState, action:
    | FetchCitySusccess
    | FetchCountriesSuccess
    | SelectCity) {
    
    switch (action.type) {
        case CustomersActionTypes.FETCH_COUNTRIES_SUCCESS:
            return { ...state, countries: action.payload};
        
        case CustomersActionTypes.FETCH_CITY_SUCCESS:
            const cities = { ...state.cities, [action.payload.Id]: action.payload };
            return { ...state, cities};
        
        case CustomersActionTypes.SELECT_CITY:
            return { ...state, selectedCity: action.payload };    
    }
    
    return state;
}

export const customerFeatureSelector = createFeatureSelector('customer');

export const CountriesSelector = createSelector(customerFeatureSelector, (state: ICustomersInitialState) => state.countries);

export const CitiesSelector = createSelector(customerFeatureSelector, (state: ICustomersInitialState) => state.cities);

export const SelectedCitySelector = createSelector(customerFeatureSelector, (state: ICustomersInitialState) => state.selectedCity);

export const ActiveCitySelector = createSelector(CitiesSelector, SelectedCitySelector, (cities, selectedCity) => cities[selectedCity]);
