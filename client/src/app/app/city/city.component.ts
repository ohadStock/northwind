import { SelectCity } from './../actions/customers.actions';
import { Component, ViewChild, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ICustomersInitialState, ActiveCitySelector, Customer } from '../reducers/customers.reducer';
import { Store } from '@ngrx/store';
import { filter, tap } from 'rxjs/operators';

declare const google: any;


@Component({
  selector: 'app-city',
  templateUrl: `./city.component.html`,
  styleUrls: ['./city.component.css']
})
export class CityComponent implements   AfterViewInit {
  @ViewChild('gmap') gmapElement: any;
  // @ts-ignore
  map: google.maps.Map;
  
  public customer: Customer;
  public citySelector$ = this.store.select(ActiveCitySelector).pipe(
    filter(city => Boolean(city) && city.Id === this.customerId)
  );
  public customerId: string;
  
  constructor(private route: ActivatedRoute, public store: Store<ICustomersInitialState>) { 
    this.route.params.subscribe(params => {
      this.customerId = params.id;
      this.store.dispatch(new SelectCity(params.id));
    });

    this.citySelector$.subscribe(customer => {
      
      this.customer = customer;
      
      // @ts-ignore
      const geocoder = new google.maps.Geocoder();
      const address = `${this.customer.Address}, ${this.customer.City} ${this.customer.Country}`;
      
      geocoder.geocode({address}, this.setMapCenter.bind(this));
    });
  }

  ngAfterViewInit() {
    const mapProp = {
      // @ts-ignore
      center: new google.maps.LatLng(18.5793, 73.8143),
      zoom: 13,
      // @ts-ignore
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    // @ts-ignore
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);

  }

  setMapCenter(results, status) {
    if (status === 'OK') {
      const [place] = results.filter(mapItem => mapItem.formatted_address.match(this.customer.City) !== null);
      if (place) {
        this.map.setCenter(place.geometry.location);
      }
    }
  }

}
