import { 
    FetchCity, 
    CustomersActionTypes, 
    FetchCountries, 
    FetchCitySusccess, 
    FetchCountriesSuccess, 
    SelectCity } from './../actions/customers.actions';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, flatMap, withLatestFrom, filter } from 'rxjs/operators';
import { config } from 'src/environments/environment';
import { Customer, CountriesList, CitiesSelector, ICustomersInitialState } from '../reducers/customers.reducer';

@Injectable()
export class CustomersEffect {
    @Effect()
    fetchCountries$ = this.actions$.pipe(
        ofType<FetchCountries>(CustomersActionTypes.FETCH_COUNTRIES),
        flatMap(() => {
            return this.http.get(`${config.serverUrl}/countries`).pipe(
                map((data: CountriesList) => new FetchCountriesSuccess(data))
            );
        })
    );

    @Effect()
    selectCity$ = this.actions$.pipe(
        ofType<SelectCity>(CustomersActionTypes.SELECT_CITY),
        withLatestFrom(this.store.select(CitiesSelector)),
        filter(([{payload}, cities]) => !cities[payload]),
        map(([{payload}]) => new FetchCity(payload))
    );

    @Effect()
    fetchCity$ = this.actions$.pipe(
        ofType<FetchCity>(CustomersActionTypes.FETCH_CITY),
        flatMap(({id}) => {
               return this.http.get(`${config.serverUrl}/city/${id}`).pipe(
                   map((data: Customer) => new FetchCitySusccess(data))
               );
        })
    );

    constructor(public actions$: Actions, public http: HttpClient, public store: Store<ICustomersInitialState>) {}
}
