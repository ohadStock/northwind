"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (data) => {
    class Customers {
        constructor(data) {
            this._countries = {};
            this._cities = {};
            this.init(data);
        }
        get countries() {
            return this._countries;
        }
        init(data) {
            data.forEach((item) => {
                if (!this._countries[item.Country]) {
                    this._countries[item.Country] = [];
                }
                this._countries[item.Country].push({ id: item.Id, name: item.City });
                this._cities[item.Id] = item;
            });
        }
        city(id) {
            return this._cities[id];
        }
    }
    return new Customers(data);
};
//# sourceMappingURL=customers.js.map