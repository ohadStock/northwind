"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const cors_1 = __importDefault(require("cors"));
const data = __importStar(require("../data-source/northwind-customers.json"));
const customers_1 = __importDefault(require("./customers/customers"));
const app = express_1.default();
app.use(cors_1.default());
const customers = customers_1.default(data.Customers);
app.set('port', process.env.PORT || 3000);
app.get('/', (req, res) => {
    res.send('hello world');
    res.end();
});
app.get('/countries', (req, res) => {
    res.send(customers.countries);
    res.end();
});
app.get('/city/:id', (req, res) => {
    res.send(customers.city(req.params.id));
    res.end();
});
const server = app.listen(app.get('port'), () => {
    console.log('App is running at http://localhost:%d in mode', app.get('port'));
});
exports.default = server;
//# sourceMappingURL=index.js.map