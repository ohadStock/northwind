import express from 'express';
import cors from 'cors';
import * as data from '../data-source/northwind-customers.json';
import Customers from './customers/customers';

const app = express();

app.use(cors());

const customers = Customers(data.Customers as Customer[]);

app.set('port', process.env.PORT || 3000);

app.get('/countries', (req, res) => {
    res.send(customers.countries);
    res.end();
});

app.get('/city/:id', (req, res) => {
    res.send(customers.city(req.params.id));
    res.end();
});

const server = app.listen(app.get('port'), () => {
    console.log('App is running at http://localhost:%d in mode', app.get('port'));
});

export default server;