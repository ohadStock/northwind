export default (data: Customer[]) => {
    
    class Customers {
        private _countries: { [key: string]: { id: string, name: string } []} = {};
        private _cities: {[key: string]: Customer} = {}; 
        
        constructor(data: Customer[]) {
            this.init(data);
        }
        
        get countries (){
            return this._countries;
        }
        
        init(data: Customer[]) {
           data.forEach((item) => {
               if (!this._countries[item.Country]) {
                   this._countries[item.Country] = [];
               }
               this._countries[item.Country].push({id: item.Id, name: item.City});
               this._cities[item.Id]  = item;
           });     
        }

        city(id: string) {
            return this._cities[id];
        }
    }
    
    return new Customers(data);
};
